import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../data-types/user';
import { UserService } from '../user.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  private users: Array<User>;
  private spinner = false;

  constructor(
    private userService: UserService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user) {
      this.router.navigate(['/class']);
    }
  }

  public credential = new FormGroup({
    email: new FormControl('', [
      Validators.required,
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });

  ngOnInit() {
  }

  signIn() {
    this.userService.getUser().subscribe(users => {
      if (users.length > 0) {
        const userIndex = users.findIndex(user =>
          user.email === this.credential.value.email && user.password === this.credential.value.password
        );
        if (userIndex !== -1) {
          this.credential.disable();
          this.spinner = true;
          localStorage.setItem('user', JSON.stringify(users[userIndex]));
          this.snackBar.open('Log In Successfull, Redirecting.. Please Wait');
          setTimeout(() => {
            this.spinner = false;
            this.router.navigate(['/class']);
          }, 2000);
        } else {
          this.snackBar.open('Email or Password incorrect, Please try again..!!');
        }
      }
    });
  }

}
