export interface Enrolment {
    userId: number;
    classId: number;
}
