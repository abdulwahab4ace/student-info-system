import { Component, OnInit } from '@angular/core';
import { ClassService } from '../class.service';
import { Class } from '../data-types/class';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Enrolment } from '../data-types/enrolment';
import { User } from '../data-types/user';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-classes-list',
  templateUrl: './classes-list.component.html',
  styleUrls: ['./classes-list.component.css']
})
export class ClassesListComponent implements OnInit {

  public classes: Array<Class>;
  public enrolments: Array<Enrolment>;

  public user: User = JSON.parse(localStorage.getItem('user'));

  public projectControl = new FormControl();
  public filteredClasses: Observable<Array<Class>>;

  constructor(
    private classService: ClassService
  ) {
  }

  ngOnInit() {
    this.classService.getEnrolments().subscribe(enrolments => {
      this.enrolments = enrolments;
    });

    this.classService.getClasses().subscribe(classes => {
      if (classes.length > 0) {
        this.classes = classes;
        this.filteredClasses = this.projectControl.valueChanges.pipe(
          startWith(''),
          map(value => this._filter(value))
        );
      } else {

      }
    });
  }

  private _filter(value: any): Array<Class> {
    const filterValue = value.toLowerCase();
    return this.classes.filter(classes => classes.title.toLowerCase().includes(filterValue.trim()));
  }

  enrol(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to Enrol in this course!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        const enrol: Enrolment = { userId: this.user.id, classId: id };
        this.enrolments.push(enrol);
        Swal.fire(
          'Enrolled!',
          'In this course!',
          'success'
        );
      }
    });
  }

  cancelEnrolment(id) {

    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to cancel Enrollment in this course!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirm'
    }).then((result) => {
      if (result.value) {
        const index = this.enrolments.findIndex(enrol => enrol.userId === this.user.id && enrol.classId === id);
        this.enrolments.splice(index, 1);
        Swal.fire(
          'Enrolment Canceled!',
          'In this course!',
          'success'
        );
      }
    });
  }

  checkIfEnrolled(classId) {
    const index = this.enrolments.findIndex(e => e.userId === this.user.id && e.classId === classId);
    return index !== -1 ? true : false;
  }

}
