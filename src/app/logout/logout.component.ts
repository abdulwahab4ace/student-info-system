import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../data-types/user';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  private user: User;

  constructor(private router: Router) {
    const user = JSON.parse(localStorage.getItem('user'));
    if (!user) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  logOut() {
   localStorage.removeItem('user');
   this.router.navigate(['/']);
  }

}
